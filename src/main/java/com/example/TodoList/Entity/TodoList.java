package com.example.TodoList.Entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "TodoList")
public class TodoList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "todoId" )
    private Long todoId;

    @Column(name = "topic")
    private String topic;

    @Size(max=20)
    @Column(name = "title")
    private String title;

    @Size(max=50)
    @Column(name = "description")
    private String description;

    @CreationTimestamp
    @Column(name = "dueDate")
    private LocalDateTime dueDate;

    @Column(name = "completed")
    private boolean completed;


}
