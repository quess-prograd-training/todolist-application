package com.example.TodoList.Service;

import com.example.TodoList.Entity.TodoList;
import com.example.TodoList.Repository.TodoListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoListServiceImpl implements TodoListService{
    @Autowired
    TodoListRepository todoListRepository;

    @Override
    public List<TodoList> getTodoLists(){
        return todoListRepository.findAll();
    }

    @Override
    public Optional<TodoList> getTodoListById(Long todoId){
        return todoListRepository.findById(todoId);
    }
    @Override
    public TodoList createTodoList(TodoList todoList){
        return todoListRepository.save(todoList);
    }

    @Override
    public TodoList updateTodoList(Long todoId, TodoList todoList){
        TodoList fetchedTodoList=todoListRepository.findById(todoId).orElseThrow();
        fetchedTodoList.setTopic(todoList.getTopic());
        fetchedTodoList.setTitle(todoList.getTitle());
        fetchedTodoList.setDescription(todoList.getDescription());
        fetchedTodoList.setDueDate(todoList.getDueDate());
        fetchedTodoList.setCompleted(todoList.isCompleted());
        return todoListRepository.save(fetchedTodoList);
    }

    @Override
    public String deleteTodoListById(Long todoId){
        TodoList todoListObj = todoListRepository.findById(todoId).orElseThrow();
        todoListRepository.delete(todoListObj);
        return "TodoListId-" +todoId+ "deleted";
    }




}
