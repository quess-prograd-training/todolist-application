package com.example.TodoList.Service;

import com.example.TodoList.Entity.TodoList;

import java.util.List;
import java.util.Optional;


public interface TodoListService {

    List<TodoList> getTodoLists();

    TodoList createTodoList(TodoList todoList);

    Optional<TodoList> getTodoListById(Long todoId);

    TodoList updateTodoList(Long todoId, TodoList todoList);

    String deleteTodoListById(Long todoId);
}
