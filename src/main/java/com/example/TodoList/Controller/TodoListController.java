package com.example.TodoList.Controller;

import com.example.TodoList.Entity.TodoList;
import com.example.TodoList.Service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/TodoLists")
public class TodoListController {

    @Autowired
    private TodoListService todoListService;

    @GetMapping
    public List<TodoList> getTodoLists(){
        return todoListService.getTodoLists();
    }

    @GetMapping("/{todoId}")
    public Optional<TodoList> getTodoListById(@PathVariable Long todoId){
        return todoListService.getTodoListById(todoId);

    }

    @PostMapping
    public TodoList createTodoList(@RequestBody TodoList todoList){
        return todoListService.createTodoList(todoList);
    }

    @PutMapping("/{todoId}")
    public TodoList updateTodoList(@PathVariable Long todoId,@RequestBody TodoList todoList){
        return todoListService.updateTodoList(todoId,todoList);
    }

    @DeleteMapping("/{todoId}")
    public String deleteTodoListById(@PathVariable Long todoId){
        return todoListService.deleteTodoListById(todoId);
    }
}
